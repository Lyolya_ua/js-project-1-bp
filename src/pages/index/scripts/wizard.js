import { saveToStorage } from './login';

const user = {};

// blocks
const loginBlock = document.getElementById('loginBlock');
const step1Block = document.getElementById('step1Block');
const regBlock = document.getElementById('regBlock');

// buttons
const regBtn = document.getElementById('regBtn');
const toLoginSvg = document.getElementById('toLoginSvg');
const from1to2 = document.getElementById('from1to2');
const toStep2Btn = document.getElementById('toStep2Btn');
const from2to1 = document.getElementById('from2to1');
const from3to2Svg = document.getElementById('from3to2Svg');
const createAccountBtn = document.getElementById('createAccount');

regBtn.addEventListener('click', (e) => {
    showBlocks(loginBlock, step1Block);
});

toStep2Btn.addEventListener('click', (e) => {
    showBlocks(step1Block, regBlock);
});

toLoginSvg.addEventListener('click', (e) => {
    showBlocks(step1Block, loginBlock);
});

from3to2Svg.addEventListener('click', (e) => {
    showBlocks(regBlock, step1Block);
});

from2to1.addEventListener('click', (e) => {
    showBlocks(regBlock, step1Block);
});

from1to2.addEventListener('click', (e) => {
    showBlocks(step1Block, loginBlock);
});

createAccountBtn.addEventListener('click', (e) => {
    createAccount();
});

function showBlocks(block1, block2) {
    block1.style.display = 'none';
    block2.style.display = 'flex';
}

function createAccount () {
    const name = document.getElementById('name');
    const nameValue = name.value.trim();
    const isNameValue = getName(nameValue);
    const email = document.getElementById('email');
    const emailValue = email.value.trim();
    const isEmail = emailValue.indexOf('@') > 1 ? true : false;
    const password = document.getElementById('password');
    const passwordValue = password.value.trim();
    const passwordNext = document.getElementById('password_next');
    const passwordNextValue = passwordNext.value.trim();

    const typeInput = step1Block.querySelector('form input:checked');
    const type = typeInput.id === 'user_teacher' ? 'teacher' : 'student';

    user.type = type;

    getError();

    if(!isNameValue || !isEmail || passwordValue === '' || passwordValue !== passwordNextValue) {
        if(!isNameValue) {
            name.classList.add('error');
            console.error('Введите валидное имя и фамилию');
        }

        if(!isEmail) {
            email.classList.add('error');
            console.error('Введите валидный email');
        }

        if (passwordValue === '' || passwordValue !== passwordNextValue) {
            password.classList.add('error');
            passwordNext.classList.add('error');
            console.error('Введите валидный пароль');
        }
    } else {
        getError();

        user.name = nameValue;
        user.email = emailValue;
        user.password = passwordValue;

        if(user.type === 'teacher') {
            localStorage.setItem('teacher', JSON.stringify(user));
            saveToStorage(user);
        } else {
            const students = JSON.parse(localStorage.getItem('students')) || [];
            localStorage.setItem('students', JSON.stringify([user, ...students]));
            saveToStorage(user);
        }

        regBlock.querySelector('form').submit();
        regBlock.querySelector('form').reset();

        window.location.href = user.type === 'teacher' ? 'teacher.html' : 'student.html';
    }

    function getName(name) {
        const array = name.split(' ');
        return array.length > 1 ? true : false;
    }

    function getError() {
        const error = document.querySelectorAll('.error');
        error.forEach(item => item.classList.remove('error'));
    }
}
