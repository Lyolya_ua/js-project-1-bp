const bookedLessons = JSON.parse(localStorage.getItem('lessons')) || [];
const scheduledLessonsBlock = document.querySelector('.block__scheduled-lessons');

function getLesson(lesson) {
    const { name, title, time, tomorrow, duration } = lesson;
    const fromTime = time + ':00';
    const toTime = (duration === 120) ? (time + 2) + ':00' : (duration === 90) ? (time + 1) + ':30' : (time + 1) + ':00' ;

    return `
        <div class="card-box">
            <div class="card-illustration">
                <img src="./images/user_02.png" alt=""></div>
            <div class="info">
                <p class="sub-title">${tomorrow ? 'Завтра' : 'Сегодня'}, ${fromTime} - ${toTime}</p>
                <p class="info-title"${name}</p>
                <p class="info-desc">${title}</p>
            </div>
        </div>
    `;
}

function generateHTML(lessons) {
    const lessonsHTML = lessons.map(lesson => {
        return getLesson(lesson);
    }).join('');

    scheduledLessonsBlock.insertAdjacentHTML('afterend', lessonsHTML);
}

generateHTML(bookedLessons);
