import { timeSlots, lessons as lessonObj } from './constants';

const student = JSON.parse(localStorage.getItem('login')).name;
const formLessons = document.getElementById('formLessons');
const submitBtn = formLessons.querySelector('.button');

submitBtn.addEventListener('click', (e) => {
    e.preventDefault();

    const blockTypes = formLessons.querySelector('.block-types');
    const blockSlot = formLessons.querySelector('.block__slot');

    const typeLessonId = blockTypes.querySelector('input[type="radio"]:checked').getAttribute('id');
    const slotId = blockSlot.querySelector('input[type="radio"]:checked').getAttribute('id');
    let tomorrow = true;

    if(slotId === 'time_01' || slotId === 'time_02' || slotId === 'time_03') {
        tomorrow = false;
    }

    const lesson = {
        name: student,
        time: timeSlots[slotId],
        tomorrow: tomorrow,
    };

    for(let key in lessonObj[typeLessonId]) {
        lesson[key] = lessonObj[typeLessonId][key];
    }

    const lessons = JSON.parse(localStorage.getItem('lessons')) || [];
    localStorage.setItem('lessons', JSON.stringify([lesson, ...lessons]));
});
